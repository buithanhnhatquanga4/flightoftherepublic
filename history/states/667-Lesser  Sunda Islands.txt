
state={
	id=667
	name="STATE_667"
	resources={
		rubber=12.000
	}

	history={
		owner = INS
		add_core_of = INS
		victory_points = {
			4273 5 
		}
		buildings = {
			infrastructure = 3
			4273 = {
				naval_base = 2

			}
			10175 = {
				naval_base = 1

			}
			12234 = {
				naval_base = 1

			}

		}

	}

	provinces={
		1298 4301 7321 10175 10191 10219 12151 12162 12219 12234 
	}
	manpower=2959920
	buildings_max_level_factor=1.000
	state_category=large_town
}
