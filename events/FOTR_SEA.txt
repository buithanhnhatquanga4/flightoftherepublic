﻿add_namespace = FOTR_SEA

country_event = {
	id = FOTR_SEA.0 #For New Sweden/Nordnamibia
	title = FOTR_SEA.0.t #Sweden demands we implement taxes!
	desc = FOTR_SEA.0.d

	is_triggered_only = yes

	immediate = {
		set_country_flag = FOTR_SEA_viewed
	}

	option = {
		name = FOTR_SEA.0.o2 #Why should we fund the royals?
		set_country_flag = SEA_refuse_taxes
	}
	option = {
		name = FOTR_SEA.0.o1 #Do as they wish
		set_country_flag = SEA_implement_taxes
	}
}
country_event = {
	id = FOTR_SEA.1 #For Sweden
	title = FOTR_SEA.1.t #[SEA.GetNameDef] refuses to implement the taxes!
	desc = FOTR_SEA.1.d

	is_triggered_only = yes

	option = {
		name = FOTR_SEA.1.o1
	}
	option = {
		name = FOTR_SEA.1.o2
	}
}
country_event = {
	id = FOTR_SEA.2 #For Sweden
	title = FOTR_SEA.2.t #[SEA.GetNameDef] implements the taxes!
	desc = FOTR_SEA.2.d

	is_triggered_only = yes

	option = {
		name = FOTR_SEA.2.o1
	}
	option = {
		name = FOTR_SEA.2.o2
	}
}

### REVOLUTION ###
country_event = {
	id = FOTR_SEA.3 #For Nordnamibia
	title = FOTR_SEA.3.t #Revolution!
	desc = FOTR_SEA.3.d

	is_triggered_only = yes

	option = {
		name = FOTR_SEA.3.o1 #Roars of the Revolution
		set_politics = {
			ruling_party = communism
		}
		hidden_effect = {
			add_popularity = {
				popularity = 10000
				ideology = communism
			}
			SWE = {
				transfer_state = 802
			}
			802 = {
				add_core_of = SWE
				add_core_of = SEA
			}
			every_state = {
				limit = {	
					is_owned_by = SEA
				}
				add_core_of = SEA
			}
			every_country = {
				news_event = {
					id = FOTR_SEA.4
				}
			}
		}
	}
	option = {
		name = FOTR_SEA.3.o2 #Back down
		SWE = {
			annex_country = {
				target = SEA
			}
		}
	}
}
news_event = {
	id = FOTR_SEA.4 #For Sweden
	title = FOTR_SEA.4.t #[SEA.GetNameDef] revolts!
	desc = FOTR_SEA.4.d

	is_triggered_only = yes

	option = {
		name = FOTR_SEA.4.o1 #They'll pay for this!
		trigger = {
			NOT = {
				tag = SEA
			}
			OR = {
				is_in_faction_with = SWE
				is_puppet_of = SWE
				tag = SWE
			}
		}
	}
	option = {
		name = FOTR_SEA.4.o2 #Not our issue..
		trigger = {
			NOT = {
				OR = {
					AND = {
						has_government = democratic
						SEA = {
							has_government = democratic
						}
					}
					AND = {
						has_government = communism
						SEA = {
							has_government = communism
						}
					}
					AND = {
						has_government = fascism
						SEA = {
							has_government = fascism
						}
					}
					AND = {
						has_government = neutrality
						SEA = {
							has_government = neutrality
						}
					}
				}
				tag = SEA
				OR = {
					is_in_faction_with = SWE
					is_puppet_of = SWE
					tag = SWE
				}
			}
		}
	}
	option = {
		name = FOTR_SEA.4.o3 #We wish them the best!
		trigger = {
			OR = {
				AND = {
					has_government = democratic
					SEA = {
						has_government = democratic
					}
				}
				AND = {
					has_government = communism
					SEA = {
						has_government = communism
					}
				}
				AND = {
					has_government = fascism
					SEA = {
						has_government = fascism
					}
				}
				AND = {
					has_government = neutrality
					SEA = {
						has_government = neutrality
					}
				}
			}
			NOT = {
				tag = SEA
				OR = {
					is_in_faction_with = SWE
					is_puppet_of = SWE
					tag = SWE
				}
			}
		}
	}
	option = {
		name = FOTR_SEA.4.o4 #Long Live Northern Namibia!
		trigger = {
			OR = {	
				tag = SEA
				is_in_faction_with = SEA
				is_puppet_of = SEA
			}
		}
	}
}
country_event = {
	id = FOTR_SEA.10000
	title = "Test"
	desc = "yes"

	is_triggered_only = yes
	fire_only_once = yes

	option = {
		name = "British Empire"
		ENG = {
			puppet = SEA
		}
		SEA = {
			set_party_name = { 
				ideology = neutrality 
				long_name = "British Colonial Government"
				name = "Colonial Government"
			}
			set_party_name = { 
				ideology = fascism 
				long_name = "British Colonial Government"
				name = "Colonial Government"
			}
			set_party_name = { 
				ideology = democratic 
				long_name = "British Colonial Government"
				name = "Colonial Government"
			}
			set_party_name = { 
				ideology = communism 
				long_name = "British Colonial Government"
				name = "Colonial Government"
			}
			create_country_leader = {
				name = "Rickard Sandler"
				desc = "POLITICS_RICKARD_SANDLER_DESC"
				picture = "gfx/leaders/SWE/SWE_Rickard_Sandler.dds"
				expire = "1965.1.1"
				ideology = fascism_ideology
				traits = {
					#
				}
			}
			create_country_leader = {
				name = "Rickard Sandler"
				desc = "POLITICS_RICKARD_SANDLER_DESC"
				picture = "gfx/leaders/SWE/SWE_Rickard_Sandler.dds"
				expire = "1965.1.1"
				ideology = marxism
				traits = {
					#
				}
			}
			create_country_leader = {
				name = "Rickard Sandler"
				desc = "POLITICS_RICKARD_SANDLER_DESC"
				picture = "gfx/leaders/SWE/SWE_Rickard_Sandler.dds"
				expire = "1965.1.1"
				ideology = socialism
				traits = {
					#
				}
			}
			create_country_leader = {
				name = "Rickard Sandler"
				desc = "POLITICS_RICKARD_SANDLER_DESC"
				picture = "gfx/leaders/SWE/SWE_Rickard_Sandler.dds"
				expire = "1965.1.1"
				ideology = despotism
				traits = {
					#
				}
			}
		}
		if = {
			limit = {
				has_dlc = "Together for Victory"
			}
			if = {
				limit = {
					NMB = {
						OR = {
							is_puppet_of = ENG
							is_subject_of = ENG
						}
					}
				}
				SEA = {
					set_cosmetic_tag = SEA_NORTH
				}
				NMB = {
					set_cosmetic_tag = NMB_SOUTH
				}
				else = {
					SEA = {
						set_cosmetic_tag = SEA_ENG
					}
				}
			}
			else = {
				if = {
					limit = {
						NMB = {
							is_puppet_of = ENG
						}
					}
					SEA = {
						set_cosmetic_tag = SEA_NORTH
					}
					NMB = {
						set_cosmetic_tag = NMB_SOUTH
					}
					else = {
						SEA = {
							set_cosmetic_tag = SEA_ENG
						}
					}
				}

			}
		}
	}
	option = {
		name = "Swedish"
		SWE = {
			puppet = SEA
		}
		SEA = {
			set_party_name = { 
				ideology = neutrality 
				long_name = SEA_colony_party_long
				name = SEA_colony_party
			}
			set_party_name = { 
				ideology = fascism 
				long_name = SEA_colony_party_long
				name = SEA_colony_party
			}
			set_party_name = { 
				ideology = democratic 
				long_name = SEA_colony_party_long
				name = SEA_colony_party
			}
			set_party_name = { 
				ideology = communism 
				long_name = SEA_colony_party_long
				name = SEA_colony_party
			}
			create_country_leader = {
				name = "Rickard Sandler"
				desc = "POLITICS_RICKARD_SANDLER_DESC"
				picture = "gfx/leaders/SWE/SWE_Rickard_Sandler.dds"
				expire = "1965.1.1"
				ideology = fascism_ideology
				traits = {
					#
				}
			}
			create_country_leader = {
				name = "Rickard Sandler"
				desc = "POLITICS_RICKARD_SANDLER_DESC"
				picture = "gfx/leaders/SWE/SWE_Rickard_Sandler.dds"
				expire = "1965.1.1"
				ideology = marxism
				traits = {
					#
				}
			}
			create_country_leader = {
				name = "Rickard Sandler"
				desc = "POLITICS_RICKARD_SANDLER_DESC"
				picture = "gfx/leaders/SWE/SWE_Rickard_Sandler.dds"
				expire = "1965.1.1"
				ideology = socialism
				traits = {
					#
				}
			}
			create_country_leader = {
				name = "Rickard Sandler"
				desc = "POLITICS_RICKARD_SANDLER_DESC"
				picture = "gfx/leaders/SWE/SWE_Rickard_Sandler.dds"
				expire = "1965.1.1"
				ideology = despotism
				traits = {
					#
				}
			}
		}
		if = {
			limit = {
				has_dlc = "Together for Victory"
			}
			if = {
				limit = {
					NMB = {
						OR = {
							is_puppet_of = SWE
							is_subject_of = SWE
						}
					}
				}
				SEA = {
					set_cosmetic_tag = SEA_NORTH_SWE
				}
				NMB = {
					set_cosmetic_tag = NMB_SOUTH_SWE
				}
				else = {
					SEA = {
						set_cosmetic_tag = SEA_SWE
					}
				}
			}
			else = {
				if = {
					limit = {
						NMB = {
							is_puppet_of = SWE
						}
					}
					SEA = {
						set_cosmetic_tag = SEA_NORTH_SWE
					}
					NMB = {
						set_cosmetic_tag = NMB_SOUTH_SWE
					}
					else = {
						SEA = {
							set_cosmetic_tag = SEA_SWE
						}
					}
				}
			}
		}
	}
}